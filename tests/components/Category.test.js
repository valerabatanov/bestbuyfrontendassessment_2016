import Category from '../../app/components/Category';

test('test category generation', ()=> {

    let category = new Category({name: 'test1', id: '1111'});
    expect(category.getValue('name')).toBe('test1');
    expect(category.getValue('id')).toBe('1111');

    expect(category.render().className).toBe('category-item');
    expect(category.render().tagName).toBe('DIV');
    expect(category.render().innerHTML).toBe('test1');
});

