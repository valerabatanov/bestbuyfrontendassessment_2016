/**
 * Ajax request utility
 *
 * @param {string} uri - The resource uri
 *
 * @returns {Promise}
 */
export default function jsonp(uri){
    return new Promise(function(resolve, reject){

        var id = '_' + Math.round(10000 * Math.random());
        var callbackName = 'jsonp_callback_' + id;
        window[callbackName] = (data)=>{
            resolve(data);
            window[callbackName] =undefined;
        };

        var ref = window.document.getElementsByTagName( 'script' )[ 0 ];
        var script = window.document.createElement( 'script' );
        script.src = uri + (uri.indexOf( '?' ) + 1 ? '&' : '?') + 'callback=' + callbackName;

        ref.parentNode.insertBefore( script, ref );

        script.onload = function () {
            this.remove();
        };
    })
}